package com.qiwenshare.common.operation.download.domain;

import lombok.Data;

@Data
public class DownloadFile {
    private String fileUrl;
//    private String timeStampName;
}
