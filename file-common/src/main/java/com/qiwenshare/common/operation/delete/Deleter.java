package com.qiwenshare.common.operation.delete;

import com.qiwenshare.common.operation.delete.domain.DeleteFile;

public abstract class Deleter {
    public abstract void delete(DeleteFile deleteFile);
}
